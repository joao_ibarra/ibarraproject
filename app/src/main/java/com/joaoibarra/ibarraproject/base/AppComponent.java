package com.joaoibarra.ibarraproject.base;

import com.joaoibarra.ibarraproject.api.ApiModule;
import com.joaoibarra.ibarraproject.features.projects.ProjectComponent;
import com.joaoibarra.ibarraproject.features.projects.ProjectModule;
import com.joaoibarra.ibarraproject.features.projects.detail.ProjectDetailComponent;
import com.joaoibarra.ibarraproject.features.projects.detail.ProjectDetailModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, ApiModule.class})
public interface AppComponent {

    void inject(BaseApplication baseApplication);

    ProjectComponent newProjectComponent(ProjectModule projectModule);

    ProjectDetailComponent newProjectDetailComponent(ProjectDetailModule projectDetailModule);
}
