package com.joaoibarra.ibarraproject.api;

import com.joaoibarra.ibarraproject.models.ProjectListResponse;
import com.joaoibarra.ibarraproject.models.TaskListResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ApiService {
    @GET("projects.json")
    Observable<ProjectListResponse> getProjectList();

    @GET("projects/{id}.json")
    Observable<ProjectListResponse> getProject(@Path("id") int projectId);

    @GET("projects/{id}/tasks.json")
    Observable<TaskListResponse> getTasksFromProject(@Path("id") int projectId);
}
