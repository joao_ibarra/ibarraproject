package com.joaoibarra.ibarraproject.features.projects.detail;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.joaoibarra.ibarraproject.R;
import com.joaoibarra.ibarraproject.base.BaseApplication;
import com.joaoibarra.ibarraproject.base.Constants;
import com.joaoibarra.ibarraproject.features.projects.ProjectListAdapter;
import com.joaoibarra.ibarraproject.models.Project;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ProjectDetailFragment extends Fragment implements ProjectDetailContract.View{

    @BindView(R.id.rv_task_list)
    RecyclerView rvTaskList;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @BindView(R.id.tv_progres_bar)
    AppCompatTextView tvProgressBar;

    Unbinder unbinder;

    @Inject
    ProjectDetailPresenter presenter;

    Project project;

    private TaskListAdapter taskListAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((BaseApplication)getActivity().getApplication())
                .getAppComponent()
                .newProjectDetailComponent(new ProjectDetailModule(this))
                .inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_project_detail, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        presenter.init();
    }


    @Override
    public void initView() {
        taskListAdapter = new TaskListAdapter(getContext(), this);
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        rvTaskList.setLayoutManager(llm);
        rvTaskList.setAdapter(taskListAdapter);
        presenter.fetchTasks();
    }

    @Override
    public void PopulateData() {
        project = getActivity().getIntent().getParcelableExtra(Constants.PROJECT_DETAIL);
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }
}
