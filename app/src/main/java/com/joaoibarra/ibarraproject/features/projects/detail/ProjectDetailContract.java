package com.joaoibarra.ibarraproject.features.projects.detail;

public interface ProjectDetailContract {
    interface View {
        void initView();

        void PopulateData();

        void showLoading();

        void hideLoading();
    }

    interface Presenter {
        void init();

        void getProject(String id);

        void fetchTasks();
    }
}
