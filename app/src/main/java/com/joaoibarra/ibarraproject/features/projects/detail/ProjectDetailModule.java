package com.joaoibarra.ibarraproject.features.projects.detail;

import com.joaoibarra.ibarraproject.features.projects.ProjectScope;

import dagger.Module;
import dagger.Provides;

@Module
public class ProjectDetailModule {
    private final ProjectDetailContract.View projectDetailView;

    public ProjectDetailModule(ProjectDetailContract.View projectDetailView) {
        this.projectDetailView = projectDetailView;
    }

    @Provides
    @ProjectScope
    ProjectDetailPresenter provideMovieDetailPresenter() {
        return new ProjectDetailPresenter(projectDetailView);
    }
}
