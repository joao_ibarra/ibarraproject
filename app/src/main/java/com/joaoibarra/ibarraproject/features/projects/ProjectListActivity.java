package com.joaoibarra.ibarraproject.features.projects;

import android.os.Bundle;

import com.joaoibarra.ibarraproject.R;
import com.joaoibarra.ibarraproject.base.BaseActivity;

public class ProjectListActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_list);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }

    }
}