package com.joaoibarra.ibarraproject.features.projects.detail;

import com.joaoibarra.ibarraproject.features.projects.ProjectScope;

import dagger.Subcomponent;

@ProjectScope
@Subcomponent(modules = {ProjectDetailModule.class})
public interface ProjectDetailComponent {
    void inject(ProjectDetailFragment projectDetailFragment);
}
