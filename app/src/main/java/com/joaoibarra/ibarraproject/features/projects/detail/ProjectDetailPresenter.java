package com.joaoibarra.ibarraproject.features.projects.detail;

public class ProjectDetailPresenter implements ProjectDetailContract.Presenter {

    private ProjectDetailContract.View projectDetailView;

    public ProjectDetailPresenter(ProjectDetailContract.View projectDetailView) {
        this.projectDetailView = projectDetailView;
    }

    @Override
    public void init() {
        projectDetailView.PopulateData();
    }

    @Override
    public void getProject(String id) {
       /* databaseInteractor.getMovieData(id).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.computation())
                .subscribe(projectDetailView::onCompleted, this::onFailed);*/
    }

    @Override
    public void fetchTasks() {

    }
}