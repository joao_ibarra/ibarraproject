package com.joaoibarra.ibarraproject.features.projects;

import javax.inject.Scope;

@Scope
public @interface ProjectScope {
}
