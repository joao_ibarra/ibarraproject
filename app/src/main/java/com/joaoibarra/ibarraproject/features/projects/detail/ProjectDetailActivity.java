package com.joaoibarra.ibarraproject.features.projects.detail;

import android.os.Bundle;

import com.joaoibarra.ibarraproject.R;
import com.joaoibarra.ibarraproject.base.BaseActivity;

public class ProjectDetailActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_detail);
    }
}
